#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int main()
{
    int _t;
    cin >> _t;
    for(int _i = 0; _i != _t; ++_i)
    {
        int n;
        string a, b;
        cin >> n >> a >> b;
        for(int i = 0; i <= n/2; ++i)
        {
            int length = 0;
            int j = 0, k = i;
            while(k != n)
            {
                if((a[j] - b[k]) <= 1 && -1 <= (a[j] - b[k]))
                {
                    ++length;
                    if(length == (n+1)/2)
                    {
                        cout << "POSITIVE" << endl;
                        goto done;
                    }
                }
                else
                    length = 0;
                ++j;
                ++k;
            }
            length = 0;
            j = i;
            k = 0;
            while(j != n)
            {
                if((a[j] - b[k]) <= 1 && -1 <= (a[j] - b[k]))
                {
                    ++length;
                    if(length == (n+1)/2)
                    {
                        cout << "POSITIVE" << endl;
                        goto done;
                    }
                }
                else
                    length = 0;
                ++j;
                ++k;
            }
        }
        cout << "NEGATIVE" << endl;
        done:;
    }
}