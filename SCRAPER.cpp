/*
 * Max-Height: F
 * Two elevators: (X₀, Y₀) and (X₁, Y₁)
 * WLOG: X₀ ≤ X₁
 * d := X₁ - X₀
 * Y₀·f ≡ d (Y₁)
 * 
 * a·Y₀ + b·Y₁ = gcd(Y₀, Y₁) = g
 * Unless g|d: f does not exist
 * Y₀/g·a ≡ 1 (Y₁)
 * Y₀/g·a·d ≡ d
 * f ≡ a·d/g
 * 
 * Result: X₁ ≤ X₀+Y₀·f+n·lcm(Y₀,Y₁) < F (X₁ ≤ F-1 - ((F-1 - (X₀+Y₀·f)) % lcm(Y₀,Y₁)))
 */


#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <cstring>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cassert>
#include <sys/time.h>
#include <stack>
#include <vector>
#include <set>
#include <list>
#include <ctime>
#include <tr1/unordered_map>
using namespace std;
using namespace tr1;

namespace
{

typedef unsigned long long ull;
typedef int ll; // once it did not work with 32-bit ints, but now it works

inline FILE& operator>>(FILE& f, char*& d)
{
    int s = 20;
    d = (char*)malloc(s);
    int chr;
    int i = 0;
    do
    {
        chr = fgetc(&f);
        if(chr == EOF)
            goto OPERATOR_RSHIFT_FILE_CHAR_PTR_end;
    }
    while(chr == '\n' || chr == '\r' || chr == '\t' || chr == ' ');
    do
    {
        if(i == s)
        {
            s *= 2;
            d = (char*)realloc(d, s);
        }
        d[i] = chr;
        chr = fgetc(&f);
        ++i;
    }
    while(chr != EOF && chr != '\n' && chr != '\r' && chr != '\t' && chr != ' ');
    OPERATOR_RSHIFT_FILE_CHAR_PTR_end:;
    d = (char*)realloc(d, i+1);
    d[i] = '\0';
    return f;
}
inline FILE& operator>>(FILE& f, char& chr)
{
    int x;
    do
    {
        x = fgetc(&f);
        if(x == EOF)
        {
            chr = '\0';
            return f;
        }
    }
    while(x == '\n' || x == '\r' || x == '\t' || x == ' ');
    chr = x;
    return f;
}
inline FILE& operator>>(FILE& f, int& x)
{
    char *d;
    f >> d;
    x = atoi(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, ull& x)
{
    fscanf(&f, "%llu", &x);
    return f;
}
inline FILE& operator>>(FILE& f, double x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, long double& x)
{
    fscanf(&f, "%LE", &x);
    return f;
}
inline FILE& operator>>(FILE& f, string& str)
{
    char *d;
    f >> d;
    str.~string();
    new (&str) string(d);
    free(d);
    return f;
}
inline FILE& operator<<(FILE& f, const char *str)
{
    fputs(str, &f);
    return f;
}
inline FILE& operator<<(FILE& f, int x)
{
    fprintf(&f, "%d", x);
    return f;
}
inline FILE& operator<<(FILE& f, ull x)
{
    fprintf(&f, "%llu", x);
    return f;
}
inline FILE& operator<<(FILE& f, long long x)
{
    fprintf(&f, "%lld", x);
    return f;
}
inline FILE& operator<<(FILE& f, double x)
{
    fprintf(&f, "%E", x);
    return f;
}
inline FILE& operator<<(FILE& f, long double x)
{
    fprintf(&f, "%LE", x);
    return f;
}
inline FILE& operator<<(FILE& f, const string& str)
{
    f << str.c_str();
    return f;
}
inline FILE& operator<<(FILE& f, char c)
{
    fputc(c, &f);
    return f;
}
struct _endofline
{
} eol;
struct _flush
{
} clearbuff;
inline FILE& operator<<(FILE& f, const __typeof__(eol)&)
{
    fputc('\n', &f);
    fflush(&f);
    return f;
}
inline FILE& operator<<(FILE& f, const __typeof__(clearbuff)&)
{
    fflush(&f);
    return f;
}

FILE& lin(*stdin);  // low-level-in
FILE& lout(*stdout);    // low-level-out
FILE& lerr(*stderr);    // low-level-err

typedef pair<ll,ll> PII;
typedef pair<ll,PII > PIII;

template<typename T>
inline T pred(T t)
{
    --t;
    return t;
}
template<typename T>
inline T succ(T t)
{
    ++t;
    return t;
}

}

inline ll MOD(ll x, ll y)
{
    ll m = x % y;
    if(m < 0)
        return y + m;
    return m;
}

unordered_map<ll, unordered_map<ll, PIII> > eeavalue;

PIII eea(ll y0, ll y1)
{
    if(eeavalue[y0].count(y1))
        return eeavalue[y0][y1];
    if(y1 == 1)
        return eeavalue[y0][y1] = make_pair(1, make_pair(0, 1));
    if(y0 == 1)
        return eeavalue[y0][y1] = make_pair(1, make_pair(1, 0));
    if(y0 == y1)
        return eeavalue[y0][y1] = make_pair(y0, make_pair(1, 0));
    if(y1 == 0)
        return eeavalue[y0][y1] = make_pair(y0, make_pair(1, 0));
    PIII x = eea(y1, y0 % y1);
    return eeavalue[y0][y1] = make_pair(x.first, make_pair(x.second.second, x.second.first - (y0/y1) * x.second.second));
}

bool isConnected(ll F, ll x0, ll y0, ll x1, ll y1)
{
    if(x0 > x1)
        return isConnected(F, x1, y1, x0, y0);
#ifdef CONN_DEBUG
    lerr << "F: " << F << " x0: " << x0 << " y0: " << y0 << " x1: " << x1 << " y1: " << y1 << eol;
#endif
    ll d = x1 - x0;
#ifdef CONN_DEBUG
    lerr << "d: " << d << eol;
#endif
    PIII eeares = eea(y0, y1);
    ll a = eeares.second.first, b = eeares.second.second, g = eeares.first;
#ifdef CONN_DEBUG
    lerr << "a: " << a << " b: " << b << eol << "g: " << g << eol;
#endif
    if(d % g != 0)
    {
#ifdef CONN_DEBUG
        lerr << "Not divisible: " << a*d << " (" << g << ")" << eol;
#endif
        return false;
    }
    ll f = a*d/g;
#ifdef CONN_DEBUG
    lerr << "f: " << f << eol;
    lerr << (x0 + y0 * f) << eol;
    lerr << "Final check: " << F-1 - MOD((F-1 - (x0 + y0 * f)), (y0 / g * y1)) << eol;
#endif
    ll upper = F-1 - MOD((F-1 - (x0 + y0 * f)), (y0 / g * y1));
    return upper >= x1;
}

bool visited[100];
int connected[100][100];
int x[100], y[100];


int main()
{
#ifdef RAND_CHECK
    srand(143191);
    for(int i = 0; i != 1000000; ++i)
    {
        int F = rand() % 100 + 1;
        int x0 = rand() % (F), x1 = rand() % (F);
        int y0 = rand() % (F/3+1)+1, y1 = rand() % (F/3+1)+1;
        bool r = isConnected(F, x0, y0, x1, y1);
        bool rr = false;
        int s = x0, t = x1;
        while(s < F && t < F)
        {
            if(s == t)
            {
                rr = true;
                break;
            }
            if(s < t)
                s += y0;
            else
                t += y1;
        }
        if(r != rr)
        {
            lerr << "Wrong result for: " << F << ": " << x0 << " " << y0 << ", " << x1 << " " << y1 << " (" << s << ")" << eol;
        }
    }
#endif

#ifdef TEST_INPUT
    int F, x0, y0, x1, y1;
    lin >> F >> x0 >> y0 >> x1 >> y1;
    lout << isConnected(F, x0, y0, x1, y1) << eol;
#endif
    int t;
    scanf("%d", &t);
    for(int _i = 0; _i != t; ++_i)
    {
        int F, E, A, B;
        scanf("%d %d %d %d", &F, &E, &A, &B);
        for(int i = 0; i != E; ++i)
            scanf("%d %d", y+i, x+i);
        if(A == B)
            goto success;
        {
            memset(visited, 0, 100);
            memset(connected, 0, 100*100*sizeof(int));
            stack<int> search;
            for(int i = 0; i != E; ++i)
                if(x[i] <= A && (A - x[i]) % y[i] == 0)
                    search.push(i);
            while(!search.empty())
            {
                int c = search.top();
                search.pop();
                if(x[c] <= B && (B - x[c]) % y[c] == 0)
                    goto success;
                visited[c] = true;
                for(int i = 0; i != E; ++i)
                {
                    if(!visited[i])
                    {
                        bool conn;
                        if(connected[c][i] == 1)
                            conn = true;
                        else if(connected[c][i] == -1)
                            conn = false;
                        else
                        {
                            conn = isConnected(F, x[c], y[c], x[i], y[i]);
                            if(conn)
                                connected[c][i] = connected[i][c] = 1;
                            else
                                connected[c][i] = connected[i][c] = -1;
                        }
                        if(conn)
                            search.push(i);
                    }
                }
            }
        }
        puts("The furniture cannot be moved.\n");
        continue;
        success:
        puts("It is possible to move the furniture.\n");
    }
}

