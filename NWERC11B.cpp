#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int gcd(int a, int b)
{
    if(a == 0)
        return b;
    if(b == 0)
        return a;
    return gcd(b, a%b);
}

int main()
{
    int _t;
    cin >> _t;
    for(int _i = 0; _i != _t; ++_i)
    {
        int a, b;
        scanf("%d/%d", &a, &b);
        while(true)
        {
            if(a == 1 && b == 1)
                break;
            if(a > b)
            {
                cout << 'R';
                a -= b;
                swap(a, b);
            }
            else
            {
                cout << 'L';
                swap(a, b);
                a -= b;
            }
            int g = gcd(a,b);
            a /= g;
            b /= g;
//             cerr << a << "/" << b << endl;
        }
        cout << endl;
    }
}