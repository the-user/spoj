#include <iostream>
using namespace std;

int t, n, sum;
int b[1000];


int main()
{
	cin >> t;
	for(int i = 0; i != t; ++i)
	{
        cin >> n;
        int m[1000] = {};
        sum = 0;
        int p[1000] = {};
		for(int j = 0; j != n; ++j)
		{
			cin >> b[j];
			for(int k = 1; k < b[j]; ++k, ++sum)
                p[sum] = j;
		}
		for(int j = 0; j != sum; ++j)
        {
            int tmin = 1000000000;
            int pos;
            bool left;
            for(int k = 0; k != n; ++k)
            {
                if(b[k] == 0)
                {
                    int rval = 0;
                    for(int l = p[j]; l != k; ++l, __extension__({ if(l == n) l = 0; }))
                        rval += m[l] < 0 ? -1 : 1;
                    int lval = 0;
                    for(int l = k; l != p[j]; ++l, __extension__({ if(l == n) l = 0; }))
                            lval += m[l] > 0 ? -1 : 1;
                    if(lval < tmin)
                    {
                        left = true;
                        tmin = lval;
                        pos = k;
                    }
                    if(rval < tmin)
                    {
                        left = false;
                        tmin = rval;
                        pos = k;
                    }
                }
            }
            if(left)
            {
                for(int l = pos; l != p[j]; ++l, __extension__({ if(l == n) l = 0; }))
                    --m[l];
            }
            else
            {
                for(int l = p[j]; l != pos; ++l, __extension__({ if(l == n) l = 0; }))
                    ++m[l];
            }
            --b[p[j]];
            b[pos] = 1;
        }
        int nm = 0;
        for(int j = 0; j != n; ++j)
        {
//             cerr << " " << m[j];
            nm += (m[j] < 0 ? -m[j] : m[j]);
        }
//         cerr << endl;
        cout << nm << endl;
	}
}


