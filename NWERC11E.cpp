#include <iostream>
using namespace std;

typedef long long ll;

int main()
{
    int _t;
    cin >> _t;
    for(int _i = 0; _i != _t; ++_i)
    {
        int spamnumber;
        cin >> spamnumber;
        string str;
        cin >> str;
        ll cnt[256] = {};
        int last[256] = {};
//         bool used[256] = {};
        for(int i = 0; i != str.size(); ++i)
        {
//             used[str[i]] = true;
            ++cnt[str[i]];
            last[str[i]] = i;
        }
        ll saved = 0;
        ll pos = str.size()-1;
        for(int i = str.size()-1; i >= 0; --i)
        {
            if(i == last[str[i]])
            {
                saved += cnt[str[i]]*(i-pos);
                pos -= cnt[str[i]];
            }
        }
        cout << 5*saved << endl;
    }
}