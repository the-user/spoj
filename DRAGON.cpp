#include <iostream>
#include <vector>
 
using namespace std;
 
int n, m, k;
 
struct node
{
    vector<node*> children;
    vector<int> ill;
    vector<int> grB, grN; /* biggest and non-biggest */
    int size;
};
 
node *root;
 
vector<vector<pair<int,int> > > adj;
vector<bool> visited;
 
node *buildTree(int x)
{
    if(visited[x])
        return 0;
    node *ret = new node;
    visited[x] = true;
    ret->size = 1;
    for(int i = 0; i != adj[x].size(); ++i)
    {
        if(!visited[adj[x][i].first])
        {
            ret->children.push_back(buildTree(adj[x][i].first));
            ret->ill.push_back(adj[x][i].second);
            ++ret->size;
        }
    }
    return ret;
}
 
void checkPart(node *d, bool b, int sum, int rem, vector<int>& p)
{
    if(rem < 0)
        return;
    if(p.size() == d->children.size())
    {
        if(rem == 0)
        {
            if(b)
            {
                int val = 0;
                for(int i = 0; i != d->children.size(); ++i)
                    val += min(d->children[i]->grB[p[i]] + d->ill[i], d->children[i]->grN[p[i]]);
                if(val < d->grB[sum+1])
                    d->grB[sum+1] = val;
            }
            else
            {
                int val = 0;
                for(int i = 0; i != d->children.size(); ++i)
                    val += min(d->children[i]->grB[p[i]], d->children[i]->grN[p[i]] + d->ill[i]);
                if(val < d->grN[sum])
                    d->grN[sum] = val;
            }
        }
    }
    else
    {
        p.push_back(0);
        for(; p.back() <= rem; ++p.back())
            checkPart(d, b, sum, rem - p.back(), p);
        p.pop_back();
    }
}
 
void build2opts(node *d)
{
    if(d->size == 1)
    {
        d->grB.resize(2);
        d->grN.resize(2);
        d->grB[0] = d->grN[1] = 1000000000;
        d->grB[1] = d->grN[0] = 0;
    }
    else
    {
        d->grN.resize(d->size+1, 1000000000);
        d->grB.resize(d->size+1, 1000000000);
        for(int i = 0; i != d->children.size(); ++i)
        {
            build2opts(d->children[i]);
        }
        vector<int> p;
        for(int i = 0; i != d->size; ++i)
        {
            checkPart(d, false, i, i, p);
            checkPart(d, true, i, i, p);
        }
    }
}
 
void check3Part(node *d, bool b, int sum, int rem, vector<int>& p)
{
    if(rem < 0)
    return;
    if(p.size() == d->children.size())
    {
        if(rem == 0)
        {
            if(b)
            {
                int val = 0;
                for(int i = 0; i != d->children.size(); ++i)
                    val += min(d->children[i]->grB[p[i]] + d->ill[i], d->children[i]->grN[p[i]]);
                if(val < d->grB[sum+1])
                    d->grB[sum+1] = val;
            }
            else
            {
                int val = 0;
                for(int i = 0; i != d->children.size(); ++i)
                    val += min(d->children[i]->grB[p[i]], d->children[i]->grN[p[i]]);
                if(val < d->grN[sum])
                    d->grN[sum] = val;
            }
        }
    }
    else
    {
        p.push_back(0);
        for(; p.back() <= rem; ++p.back())
            check3Part(d, b, sum, rem - p.back(), p);
        p.pop_back();
    }
}
 
void build3opts(node *d)
{
    if(d->size == 1)
    {
        d->grB.resize(2);
        d->grN.resize(2);
        d->grB[0] = d->grN[1] = 1000000000;
        d->grB[1] = d->grN[0] = 0;
    }
    else
    {
        d->grN.resize(d->size+1, 1000000000);
        d->grB.resize(d->size+1, 1000000000);
        for(int i = 0; i != d->children.size(); ++i)
        {
            build3opts(d->children[i]);
        }
        vector<int> p;
        for(int i = 0; i != d->size; ++i)
        {
            check3Part(d, false, i, i, p);
            check3Part(d, true, i, i, p);
        }
    }
}
 
int main()
{
    for(int _i_ = 0; _i_ != 10; ++_i_)
    {
    cin >> n >> m >> k;
    if(n < m + k - 1)
    {
        cout << "-1" << endl;
        for(int i = 1; i != n; ++i)
        {
            int x, y, ill;
            cin >> x >> y >> ill;
        }
    }
    else if(m == 1)
    {
        // Sum up illnesses
        int sum = 0;
        for(int i = 1; i != n; ++i)
        {
            int x, y, ill;
            cin >> x >> y >> ill;
            sum += ill;
        }
        cout << sum << endl;
    }
    else
    {
        adj.clear();
        adj.resize(n);
        root = 0;
        visited.clear();
        visited.resize(n, false);
        for(int i = 1; i != n; ++i)
        {
            int x, y, ill;
            cin >> x >> y >> ill;
            --x;
            --y;
            adj[x].push_back(make_pair(y, ill));
            adj[y].push_back(make_pair(x, ill));
        }
        root = buildTree(0);
        if(m == 2)
        {
            build2opts(root);
            cout << root->grB[k] << endl;
        }
        else
        {
            build3opts(root);
            cout << root->grB[k] << endl;
        }
    }
    }
} 
