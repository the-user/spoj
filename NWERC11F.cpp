#include <iostream>
#include <vector>
#include <cassert>
#include <queue>
using namespace std;

int maxFlow(vector<vector<pair<int, int> > >& network, int source, int sink)
{
    int result = 0;
    while(true)
    {
        vector<pair<int, int> > prev(network.size(), make_pair(-1,-1));
        queue<int> todo;
        todo.push(source);
        while(!todo.empty())
        {
            int nxt = todo.front();
            todo.pop();
            for(int i = 0; i != network[nxt].size(); ++i)
            {
                int nxtnxt = network[nxt][i].first;
                if(prev[nxtnxt].first == -1 /* not visited */ && network[nxt][i].second > 0 /* still usable */)
                {
                    prev[nxtnxt] = make_pair(nxt, i);
                    if(nxtnxt == sink)
                        goto foundpath;
                    todo.push(nxtnxt);
                }
            }
        }
        break;
        foundpath:
        int minonpath = 1000000000;
        for(int pos = sink; pos != source; pos = prev[pos].first)
        {
            minonpath = min(minonpath, network[prev[pos].first][prev[pos].second].second);
        }
        result += minonpath;
        for(int pos = sink; pos != source; pos = prev[pos].first)
        {
            network[prev[pos].first][prev[pos].second].second -= minonpath;
            for(int i = 0; i != network[pos].size(); ++i)
            {
                if(network[pos][i].first == prev[pos].first)
                {
                    network[pos][i].second += minonpath;
                    goto foundbackedge;
                }
            }
            network[pos].push_back(make_pair(prev[pos].first, minonpath));
            foundbackedge:;
        }
    }
    return result;
}

int main()
{
    int _t;
    cin >> _t;
    for(int _i = 0; _i != _t; ++_i)
    {
        int w, h, d, f, b;
        cin >> w >> h >> d >> f >> b;
        
        vector<vector<pair<int, int> > > network(w*h+2);
        
        for(int i = 0; i != h; ++i)
        {
            cin.get(); // eat new line
            for(int j = 0; j != w; ++j)
            {
                char c = cin.get();
                if(c == '.')
                {
                    network[w*h].push_back(make_pair(w*i+j, f));
                    if(i == 0 || i == h-1 || j == 0 || j == w-1)
                        network[w*i+j].push_back(make_pair(w*h+1, 1000000000));
                }
                else
                {
                    assert(c == '#');
                    network[w*i+j].push_back(make_pair(w*h+1, (i == 0 || i == h-1 || j == 0 || j == w-1) ? 1000000000 : d));
                }
                if(i > 0)
                    network[w*i+j].push_back(make_pair(w*(i-1)+j, b));
                if(i < h-1)
                    network[w*i+j].push_back(make_pair(w*(i+1)+j, b));
                if(j > 0)
                    network[w*i+j].push_back(make_pair(w*i+j-1, b));
                if(j < w-1)
                    network[w*i+j].push_back(make_pair(w*i+j+1, b));
            }
        }
        
        cout << maxFlow(network, w*h, w*h+1) << endl;
    }
}
