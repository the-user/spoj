#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cassert>
#include <sys/time.h>
#include <stack>
#include <vector>
#include <set>
#include <list>
#include <cmath>
using namespace std;

typedef unsigned long long ull;

inline FILE& operator>>(FILE& f, char*& d)
{
    int s = 20;
    d = (char*)malloc(s);
    int chr;
    int i = 0;
    do
    {
        chr = fgetc(&f);
        if(chr == EOF)
            goto OPERATOR_RSHIFT_FILE_CHAR_PTR_end;
    }
    while(chr == '\n' || chr == '\r' || chr == '\t' || chr == ' ');
    do
    {
        if(i == s)
        {
            s *= 2;
            d = (char*)realloc(d, s);
        }
        d[i] = chr;
        chr = fgetc(&f);
        ++i;
    }
    while(chr != EOF && chr != '\n' && chr != '\r' && chr != '\t' && chr != ' ');
    OPERATOR_RSHIFT_FILE_CHAR_PTR_end:;
    d = (char*)realloc(d, i+1);
    d[i] = '\0';
    return f;
}
inline FILE& operator>>(FILE& f, char& chr)
{
    int x;
    do
    {
        x = fgetc(&f);
        if(x == EOF)
        {
            chr = '\0';
            return f;
        }
    }
    while(x == '\n' || x == '\r' || x == '\t' || x == ' ');
    chr = x;
    return f;
}
inline FILE& operator>>(FILE& f, int& x)
{
    char *d;
    f >> d;
    x = atoi(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, ull& x)
{
    fscanf(&f, "%llu", &x);
    return f;
}
inline FILE& operator>>(FILE& f, double x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, long double& x)
{
    fscanf(&f, "%LE", &x);
    return f;
}
inline FILE& operator>>(FILE& f, string& str)
{
    char *d;
    f >> d;
    str.~string();
    new (&str) string(d);
    free(d);
    return f;
}
inline FILE& operator<<(FILE& f, const char *str)
{
    fputs(str, &f);
    return f;
}
inline FILE& operator<<(FILE& f, int x)
{
    fprintf(&f, "%d", x);
    return f;
}
inline FILE& operator<<(FILE& f, ull x)
{
    fprintf(&f, "%llu", x);
    return f;
}
inline FILE& operator<<(FILE& f, double x)
{
    fprintf(&f, "%E", x);
    return f;
}
inline FILE& operator<<(FILE& f, long double x)
{
    fprintf(&f, "%LE", x);
    return f;
}
inline FILE& operator<<(FILE& f, const string& str)
{
    f << str.c_str();
    return f;
}
inline FILE& operator<<(FILE& f, char c)
{
    fputc(c, &f);
    return f;
}
struct _endofline
{
} eol;
struct _flush
{
} clearbuff;
inline FILE& operator<<(FILE& f, const __typeof__(eol)&)
{
    fputc('\n', &f);
    fflush(&f);
    return f;
}
inline FILE& operator<<(FILE& f, const __typeof__(clearbuff)&)
{
    fflush(&f);
    return f;
}

FILE& lin(*stdin);  // low-level-in
FILE& lout(*stdout);    // low-level-out
FILE& lerr(*stderr);    // low-level-err

typedef pair<int,int> PII;

template<typename T>
inline T pred(T t)
{
    --t;
    return t;
}
template<typename T>
inline T succ(T t)
{
    ++t;
    return t;
}

int myceil(double x)
{
    if(x < 0.0000000000001)
        return 0;
    double f = floor(x), c = ceil(x);
    if(x < f + 0.0000000000001)
        return (int)f;
    return (int)c;
}

int main()
{
    int t;
    lin >> t;
    for(int _i = 0; _i != t; ++_i)
    {
        int l, p, c;
        lin >> l >> p >> c;
//         double cnt = 0;
//         while(l < p)
//         {
//             l *= c;
//             ++cnt;
//         }
//         lerr << cnt << eol;
        printf("Case #%d: %d\n", (_i+1), (myceil(log(log(double(p)/double(l))/log(c))/log(2))));
    }
}

