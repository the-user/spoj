#include <iostream>
#include <vector>
using namespace std;

int n, m;

struct node
{
    node *parent;
    int size;
    inline node() : parent(0), size(1)
    {
    }
    inline void join(node *o)
    {
        if(size < o->size)
            parent = o;
        else
            o->parent = this;
    }
    inline node *top()
    {
        if(parent)
            return parent->top();
        else
            return this;
    }
};

vector<node> nodes;

int main()
{
    cin >> n >> m;
    if(m != n-1)
        cout << "NO" << endl;
    else
    {
        nodes.reserve(n);
        for(int i = 0; i != n; ++i)
            nodes.push_back(node());
        for(int i = 0; i != m; ++i)
        {
            int x, y;
            cin >> x >> y;
            --x;
            --y;
            node *t1 = nodes[x].top();
            node *t2 = nodes[y].top();
            if(t1 == t2)
            {
                cout << "NO" << endl;
                return 0;
            }
            t1->join(t2);
        }
        cout << "YES" << endl;
    }
}
