#!/usr/bin/ruby
def binomial(n,k)
    res = 1
    for i in 1..k
        res *= (n-i+1)
        res /= i
    end
    return res
end
for n in 0..10
   for k in 1..(n-1)
       x = binomial(n,k)
       comp = %x[echo -e "1 #{x}" | ./NWERC11A]
       puts comp if comp["("+n.to_s+","+k.to_s+")"] == nil
       puts n.to_s+" "+k.to_s+"="+x.to_s if comp["("+n.to_s+","+k.to_s+")"] == nil
   end
end