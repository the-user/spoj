// f_2(x prim, n) = 2 * f_2(x+1, n) + 2 + f_1(x+1, n)
// f_2(n, n) = n prim ? 2 : 1
// f_2(x comp, n) = 2 * f_1(x+1, n) + 1
// f_1(n, n) = n prim ? 1 : 2
// f_1(x prim, n) = 2 * f_2(x+1, n) + 1
// f_1(x comp, n) = 2 * f_1(x+1, n) + f_2(x+1, n) + 2
#include <iostream>
#include <deque>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

vector<bool> isp;
vector<int> primes;

inline void sieb(int limit = 100005)
{
    isp.resize(limit, true);
    isp[0] = false;
    isp[1] = false;
    //   int end = sqrt(limit)+1;
    int end = limit;
    for(int i = 2; i <= end; ++i)
    {
        if(isp[i])
        {
            primes.push_back(i);
            for(int j = (i<<1); j < limit; j += i)
                isp[j] = false;
        }
    }
}

// typedef unsigned long long uint;
// #define uint unsigned long long
// typedef double ull;
typedef unsigned long long ull;
class HInt : public vector<uint>
{
public:
    inline HInt()
    {}
    inline HInt(const HInt& o) : vector<uint>(o)
    {}
    inline HInt(uint val)
    {
        push_back(val);
    }
    inline HInt& operator=(const HInt& o)
    {
        if(&o != this)
        {
            this->~HInt();
            new (this) HInt(o);
        }
    }
    inline void add(uint val, size_t loc)
    {
        if(loc >= size())
        {
            resize(loc+1, 0);
            (*this)[loc] = val;
        }
        else
        {
            uint oval = (*this)[loc];
            (*this)[loc] += val;
            if((*this)[loc] < oval)
                add(1, loc+1);
        }
    }
    inline HInt& operator+=(const HInt& o)
    {
        bool hasu = false;
        if(o.size() > size())
            resize(o.size(), 0);
        for(int i = 0; i != o.size(); ++i)
        {
            uint oval = (*this)[i];
            (*this)[i] += o[i];
            if(hasu)
                ++(*this)[i];
            if(hasu ? ((*this)[i] <= oval) : ((*this)[i] < oval))
                hasu = true;
            else
                hasu = false;
        }
        if(hasu)
            push_back(1);
        return *this;
    }
//     inline HInt& operator<<=(size_t s)
//     {
//         size_t m = s % sizeof(uint);
//         s /= sizeof(uint);
//         for(int i = 0; i != s; ++i)
//         {
//             push_front(0);
//         }
//         uint last = 0;
//         for(int i = s; i != size(); ++i)
//         {
//             uint nlast = ((*this)[i] >> (sizeof(uint) - m)) & (~(((uint)-1) >> (sizeof(uint) - m)));
//             (*this)[i] <<= m;
//             (*this)[i] &= (((uint)-1) << m);
//             (*this)[i] += last;
//             last = nlast;
//         }
//         if(last)
//             push_back(last);
//         return *this;
//     }
    inline void mul2()
    {
        bool last = false;
        for(int i = 0; i != size(); ++i)
        {
            bool nlast = (((*this)[i] & 0x80000000u) != 0);
            (*this)[i] *= 2;
            if(last)
                ++(*this)[i];
            last = nlast;
        }
        if(last)
            push_back(1);
    }
    inline uint operator%(uint x) const
    {
        ull r = 0;
        for(int i = size()-1; i >= 0; --i)
        {
            r *= ((ull)((uint)-1))+1;
            r += (*this)[i];
            r -= (floor(r / x)*x);
        }
        return r;
    }
    inline HInt& operator%=(uint x)
    {
        uint neu = (*this) % x;
        clear();
        push_back(neu);
        return *this;
    }
    inline HInt& operator/=(uint x)
    {
        ull r = 0;
        for(int i = size()-1; i >= 0; --i)
        {
            r *= ((ull)((uint)-1))+1;
            r += (*this)[i];
            (*this)[i] = r / (ull)x;
            r -= ((ull)(*this)[i]) * x;
        }
        while(size() && back() == 0)
            pop_back();
    }
    inline string delToString(int b = 10)
    {
        string r;
        while(size() != 0)
        {
            r += ((char)(((*this) % b) + '0'));
            (*this) /= b;
        }
        if(r.empty())
            r = "0";
        else
            reverse(r.begin(), r.end());
        return r;
    }
    inline string toString(int b = 10) const
    {
        HInt h(*this);
        return h.delToString();
    }
};



int main()
{
    sieb(5001);
    isp[1] = true;  // Nur für diese Aufgabe
    //     HInt x;
    //     x.push_back(10);
    //     x.push_back(10);
    //     cout << x.delToString() << endl;
    while(true)
    {
        int n;
        cin >> n;
        if(n == 0)
            return 0;
        HInt f1(isp[n] ? 1 : 2), f2(isp[n] ? 2 : 1), o;
        for(--n; n != 0; --n)
        {
            //             cerr << f1.toString() << " " << f2.toString() << endl;
            if(isp[n])
            {
                f2.mul2();
                o.resize(f2.size());
                for(int i = 0; i != f2.size(); ++i)
                    o[i] = f2[i];
                f2.add(2, 0);
                f2 += f1;
                f1.resize(o.size());
                for(int i = 0; i != o.size(); ++i)
                    f1[i] = o[i];
                f1.add(1, 0);
            }
            else
            {
                f1.mul2();
                o.resize(f1.size());
                for(int i = 0; i != f1.size(); ++i)
                    o[i] = f1[i];
                f1.add(2, 0);
                f1 += f2;
                f2.resize(o.size());
                for(int i = 0; i != o.size(); ++i)
                    f2[i] = o[i];
                f2.add(1, 0);
            }
        }
        //         cerr << f1.toString() << " " << f2.toString() << endl;
        cout << f2.delToString() << endl;
    }
}