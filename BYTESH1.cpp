#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <algorithm>
#include <cassert>
#include <sys/time.h>
#include <stack>
#include <vector>
#include <set>
#include <list>
using namespace std;

typedef unsigned long long ull;

typedef pair<int,int> PII;

template<typename T, typename U>
inline bool pair_sort_1(const pair<T, U>& x, const pair<T, U>& y)
{
    return x.first < y.first;
}

template<typename T, typename U>
inline bool pair_sort_2(const pair<T, U>& x, const pair<T, U>& y)
{
    return x.second < y.second;
}

int main()
{
    int t;
    scanf("%d", &t);
    vector<PII> qr;
    qr.reserve(t);
    for(int _i = 0; _i != t; ++_i)
    {
        int n;
        scanf("%d", &n);
        qr.push_back(make_pair(_i, n));
    }
    sort(qr.begin(), qr.end(), pair_sort_2<int,int>);
    vector<PII>::iterator cpos = qr.begin();
    ull plain0 = 0, plain1 = 1, l0 = 0, l1 = 0, r0 = 0, r1 = 0;
    for(int i = 1; i <= qr.back().second; ++i)
    {
        ull oplain1 = plain1, ol1 = l1, or1 = r1;
        plain1 = plain0 /* zwei längs */ + oplain1 /* eins quer */ + or1 /* ein l zum füllen */ + ol1 /* ein l zum füllen */;
        l1 = or1 /* längs versetzt */ + plain0 /* ein l */;
        r1 = ol1 /* längs versetzt */ + plain0 /* ein l */;
        plain0 = oplain1, l0 = ol1, r0 = or1;
        if(i == cpos->second)
        {
            plain0 %= 10000;
            plain1 %= 10000;
            l0 %= 10000;
            l1 %= 10000;
            r0 %= 10000;
            r1 %= 10000;
            cpos->second = plain1;
            ++cpos;
        }
        else if(plain1 >= 2000000000)
        {
            plain0 %= 10000;
            plain1 %= 10000;
            l0 %= 10000;
            l1 %= 10000;
            r0 %= 10000;
            r1 %= 10000;
        }
    }
    sort(qr.begin(), qr.end(), pair_sort_1<int, int>);
    for(int i = 0; i != qr.size(); ++i)
        printf("%d\n", qr[i].second);
}
