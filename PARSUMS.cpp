#include <cstdio>
#include <cstring>
using namespace std;

int main()
{
    short data[1000000];
    char okay[1000000];
    while(true)
    {
        int n;
        scanf("%d", &n);
        if(n == 0)
            break;
        memset(&okay[0], 1, n);
        for(int i = 0; i != n; ++i)
        {
            scanf("%hd", &data[i]);
        }
        int res = n;
        bool inneg = data[0] < 0;
        int sum = 0;
        int i = 0;
        bool onemoretime = false;
        while(true)
        {
            if(inneg)
            {
                sum += data[i];
                if(sum >= 0)
                    inneg = false;
                else
                {
                    if(!onemoretime || okay[i])
                        --res;
                    okay[i] = false;
                }
            }
            else
            {
                if(onemoretime)
                    break;
                if(data[i] < 0)
                {
                    inneg = true;
                    sum = data[i];
                    --res;
                    okay[i] = false;
                }
            }
            --i;
            if(i < 0)
                i = (i+n)%n;
            if(i == 0)
            {
                if(onemoretime)
                    break;
                onemoretime = true;
            }
        }
        
        printf("%d\n", res);
    }
}