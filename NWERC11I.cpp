#include <cstdio>
#include <set>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long int INT;

template<typename T>
inline T sqr(const T& x)
{
    return x * x;
}

struct point
{
    INT x, y;
    point()
    {}
    point(const point& o) : x(o.x), y(o.y)
    {}
    point(INT x, INT y) : x(x), y(y)
    {}
    inline INT operator*(const point& o) const
    {
        return x*o.x + y*o.y;
    }
    inline point operator+(const point& o) const
    {
        return point(x+o.x, y+o.y);
    }
    inline point operator-(const point& o) const
    {
        return point(x-o.x, y-o.y);
    }
    inline bool operator==(const point& o) const
    {
        return x == o.x && y == o.y;
    }
};

inline bool xfirst(const point& a, const point& b)
{
    return a.x < b.x || (a.x == b.x && a.y < b.y);
}

inline bool yfirst(const point& a, const point& b)
{
    return a.y < b.y || (a.y == b.y && a.x < b.x);
}

inline bool xfirstWithID(const pair<point, INT>& a, const pair<point, INT>& b)
{
    return xfirst(a.first, b.first) || (a.first == b.first && a.second < b.second);
}

inline bool yfirstWithID(const pair<point, INT>& a, const pair<point, INT>& b)
{
    return yfirst(a.first, b.first) || (a.first == b.first && a.second < b.second);
}

inline double norm(const point& p)
{
    return sqrt(double(p*p));
}

inline INT dist2(const point& a, const point& b)
{
    return (a-b)*(a-b);
}

typedef pair<point, point> lineseg;

inline double angle(const point& x, const point& y, const point& z)
{
    return acos(double((z-y)*(x-y))/norm(z-y)/norm(x-y));
}

inline bool intersect(lineseg s1, lineseg s2)
{
    static const double _2PI = 4*acos(0.0) - 0.000001; // with tolerance
    return angle(s1.first, s2.first, s1.second)
         + angle(s1.first, s2.second, s1.second)
         + angle(s2.first, s1.first, s2.second)
         + angle(s2.first, s1.second, s2.second) >= _2PI;
}

int main()
{
//     cerr << intersect(make_pair(point(0,0), point(1,1)), make_pair(point(0,1), point(1,0))) << endl;
//     cerr << intersect(make_pair(point(-4, -1), point(5, -1)), make_pair(point(0, 0), point(0,-2))) << endl;
//     cerr << intersect(make_pair(point(0,-10), point(0,0)), make_pair(point(0,-2),point(0,-8))) << endl;
    INT _t = 0;
    scanf("%lld", &_t);
    for(INT _i = 0; _i != _t; ++_i)
    {
        INT s, r, w, p;
        scanf("%lld %lld %lld %lld", &s, &r, &w, &p);
        vector<pair<point, INT> > products(p);
        vector<point> sensors(s);
        vector<lineseg> walls(w);
        for(INT i = 0; i != s; ++i)
            scanf("%lld %lld", &sensors[i].x, &sensors[i].y);
        for(INT i = 0; i != w; ++i)
            scanf("%lld %lld %lld %lld", &walls[i].first.x, &walls[i].first.y, &walls[i].second.x, &walls[i].second.y);
        for(INT i = 0; i != p; ++i)
        {
            products[i].second = i;
            scanf("%lld %lld", &products[i].first.x, &products[i].first.y);
        }
        
        sort(products.begin(), products.end(), xfirstWithID);
        sort(sensors.begin(), sensors.end(), xfirst);
        
        vector<vector<INT> > result(p);
        
        set<pair<point, INT>, typeof(&yfirstWithID)> status(&yfirstWithID);
        
        INT inprod = 0, outprod = 0, query = 0;
        
        while(outprod != p && query != s)
        {
            point& nextin = products[inprod == p ? 0 : inprod].first;
            point& nextout = products[outprod].first;
            point& nextquery = sensors[query];
            if(inprod != p && nextin.x - r <= nextquery.x && nextin.x - r <= nextout.x+r)
            {
                status.insert(products[inprod]);
                ++inprod;
            }
            else if(nextquery.x <= nextout.x + r)
            {
                pair<point, INT> lo = make_pair(point(-1000000000, nextquery.y - r), -1000000000), hi = make_pair(point(1000000000, nextquery.y + r), 1000000000);
//                 for(typeof(status.begin()) i = status.begin(); i != status.end(); ++i)
//                 {
//                     cerr << " (" << i->first.x << "," << i->first.y << ")";
//                 }
//                 cerr << endl;
                typeof(status.begin()) beg = status.lower_bound(lo), end = status.upper_bound(hi);
                for(typeof(status.begin()) i = beg; i != end; ++i)
                {
//                     cerr << "c" << " (" << nextquery.x << "," << nextquery.y << ") (" << i->first.x << "," << i->first.y << ")" << endl;
                    if(dist2(nextquery, i->first) <= sqr(r))
                    {
                        INT realr = r;
                        for(INT j = 0; j != w; ++j)
                            if(intersect(walls[j], make_pair(nextquery, i->first)))
                                --realr;
                        if(realr >= 0 && dist2(nextquery, i->first) <= sqr(realr))
                        {
//                             cerr << realr << " (" << nextquery.x << "," << nextquery.y << ") (" << i->first.x << "," << i->first.y << endl;
                            result[i->second].push_back(query);
                        }
                    }
                }
                ++query;
            }
            else
            {
                status.erase(products[outprod]);
                ++outprod;
            }
        }
        
        for(INT i = 0; i != p; ++i)
        {
            printf("%lu", result[i].size());
            for(size_t j = 0; j != result[i].size(); ++j)
                printf(" (%lld,%lld)", sensors[result[i][j]].x, sensors[result[i][j]].y);
            printf("\n");
        }
    }
}