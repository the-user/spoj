#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>
#include <stack>
#include <vector>
#include <deque>
#include <cmath>
#include <algorithm>
using namespace std;

typedef unsigned long long ull;

inline FILE& operator>>(FILE& f, char*& d)
{
    int s = 20;
    d = (char*)malloc(s);
    int chr;
    int i = 0;
    do
    {
        chr = fgetc(&f);
        if(chr == EOF)
            goto OPERATOR_RSHIFT_FILE_CHAR_PTR_end;
    }
    while(chr == '\n' || chr == '\r' || chr == '\t' || chr == ' ');
    do
    {
        if(i == s)
        {
            s *= 2;
            d = (char*)realloc(d, s);
        }
        d[i] = chr;
        chr = fgetc(&f);
        ++i;
    }
    while(chr != EOF && chr != '\n' && chr != '\r' && chr != '\t' && chr != ' ');
    OPERATOR_RSHIFT_FILE_CHAR_PTR_end:;
    d = (char*)realloc(d, i+1);
    d[i] = '\0';
    return f;
}
inline FILE& operator>>(FILE& f, char& chr)
{
    int x;
    do
    {
        x = fgetc(&f);
        if(x == EOF)
        {
            chr = '\0';
            return f;
        }
    }
    while(x == '\n' || x == '\r' || x == '\t' || x == ' ');
    chr = x;
    return f;
}
inline FILE& operator>>(FILE& f, int& x)
{
    char *d;
    f >> d;
    x = atoi(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, ull& x)
{
    fscanf(&f, "%llu", &x);
    return f;
}
inline FILE& operator>>(FILE& f, float& x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, double& x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, long double& x)
{
    fscanf(&f, "%LE", &x);
    return f;
}
inline FILE& operator>>(FILE& f, string& str)
{
    char *d;
    f >> d;
    str.~string();
    new (&str) string(d);
    free(d);
    return f;
}
inline FILE& operator<<(FILE& f, const char *str)
{
    fputs(str, &f);
    return f;
}
inline FILE& operator<<(FILE& f, int x)
{
    fprintf(&f, "%d", x);
    return f;
}
inline FILE& operator<<(FILE& f, size_t x)
{
    fprintf(&f, "%u", x);
    return f;
}
inline FILE& operator<<(FILE& f, ull x)
{
    fprintf(&f, "%llu", x);
    return f;
}
inline FILE& operator<<(FILE& f, double x)
{
    fprintf(&f, "%.12f", x);
    return f;
}
inline FILE& operator<<(FILE& f, long double x)
{
    fprintf(&f, "%.12Lf", x);
    return f;
}
inline FILE& operator<<(FILE& f, const string& str)
{
    f << str.c_str();
    return f;
}
inline FILE& operator<<(FILE& f, char c)
{
    fputc(c, &f);
    return f;
}
struct _endofline
{
} eol;
struct _flush
{
} clearbuff;
inline FILE& operator<<(FILE& f, const __typeof__(eol)&)
{
    fputc('\n', &f);
//     fflush(&f);
    return f;
}
inline FILE& operator<<(FILE& f, const __typeof__(clearbuff)&)
{
    fflush(&f);
    return f;
}

FILE& lin(*stdin);  // low-level-in
FILE& lout(*stdout);    // low-level-out
FILE& lerr(*stderr);

typedef pair<int,int> PII;

int main()
{
    int n, m;
    lin >> n >> m;
    --n; // Ignore root
    vector<vector<int> > adj(n);
    vector<int> visited(n);
    vector<bool> fauleArme(n);
    for(int i = 0; i != m; ++i)
    {
        int x, y;
        lin >> x >> y;
        if(x != n && y != n)
        {
            adj[x].push_back(y);
            adj[y].push_back(x);
        }
        else if(x == n)
            fauleArme[(y)] = true;
        else if(y == n)
            fauleArme[(x)] = true;
    }
    if(!m)
    {
        lout << "0\n";
        return 0;
    }
    for(int i = 0; i != n; ++i)
    {
        if(!visited[i])
        {
            stack<pair<int, bool> > tv;
            tv.push(make_pair(i, false));
            vector<int> currFauleArme;
            while(!tv.empty())
            {
                pair<int, bool> c = tv.top();
                tv.pop();
                if(!visited[c.first])
                {
                    if(fauleArme[(c.first)])
                        currFauleArme.push_back(c.first);
                    visited[c.first] = (c.second ? 2 : 1);
                    for(int i = 0; i != adj[c.first].size(); ++i)
                    {
                        if(visited[adj[c.first][i]] == 0)
                            tv.push(make_pair(adj[c.first][i], !c.second));
                        else if(visited[adj[c.first][i]] == (c.second ? 2 : 1))
                            goto fail;
                    }
                }
            }
            if(currFauleArme.size() > 0)
            {
                int s = visited[currFauleArme[0]];
                for(int i = 1; i != currFauleArme.size(); ++i)
                    if(visited[currFauleArme[i]] != s)
                        goto fail;
            }
        }
    }
    lout << "1\n";
    if(false)
    {
        fail:
        lout << "2\n";
    }
}

