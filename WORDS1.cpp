#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>
#include <stack>
#include <vector>
#include <deque>
#include <cmath>
#include <algorithm>
using namespace std;

typedef unsigned long long ull;

inline FILE& operator>>(FILE& f, char*& d)
{
    int s = 20;
    d = (char*)malloc(s);
    int chr;
    int i = 0;
    do
    {
        chr = fgetc(&f);
        if(chr == EOF)
            goto OPERATOR_RSHIFT_FILE_CHAR_PTR_end;
    }
    while(chr == '\n' || chr == '\r' || chr == '\t' || chr == ' ');
    do
    {
        if(i == s)
        {
            s *= 2;
            d = (char*)realloc(d, s);
        }
        d[i] = chr;
        chr = fgetc(&f);
        ++i;
    }
    while(chr != EOF && chr != '\n' && chr != '\r' && chr != '\t' && chr != ' ');
    OPERATOR_RSHIFT_FILE_CHAR_PTR_end:;
    d = (char*)realloc(d, i+1);
    d[i] = '\0';
    return f;
}
inline FILE& operator>>(FILE& f, char& chr)
{
    int x;
    do
    {
        x = fgetc(&f);
        if(x == EOF)
        {
            chr = '\0';
            return f;
        }
    }
    while(x == '\n' || x == '\r' || x == '\t' || x == ' ');
    chr = x;
    return f;
}
inline FILE& operator>>(FILE& f, int& x)
{
    char *d;
    f >> d;
    x = atoi(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, ull& x)
{
    fscanf(&f, "%llu", &x);
    return f;
}
inline FILE& operator>>(FILE& f, float& x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, double& x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, long double& x)
{
    fscanf(&f, "%LE", &x);
    return f;
}
inline FILE& operator>>(FILE& f, string& str)
{
    char *d;
    f >> d;
    str.~string();
    new (&str) string(d);
    free(d);
    return f;
}
inline FILE& operator<<(FILE& f, const char *str)
{
    fputs(str, &f);
    return f;
}
inline FILE& operator<<(FILE& f, int x)
{
    fprintf(&f, "%d", x);
    return f;
}
inline FILE& operator<<(FILE& f, size_t x)
{
    fprintf(&f, "%u", x);
    return f;
}
inline FILE& operator<<(FILE& f, ull x)
{
    fprintf(&f, "%llu", x);
    return f;
}
inline FILE& operator<<(FILE& f, double x)
{
    fprintf(&f, "%.12f", x);
    return f;
}
inline FILE& operator<<(FILE& f, long double x)
{
    fprintf(&f, "%.12Lf", x);
    return f;
}
inline FILE& operator<<(FILE& f, const string& str)
{
    f << str.c_str();
    return f;
}
inline FILE& operator<<(FILE& f, char c)
{
    fputc(c, &f);
    return f;
}
struct _endofline
{
} eol;
struct _flush
{
} clearbuff;
inline FILE& operator<<(FILE& f, const __typeof__(eol)&)
{
    fputc('\n', &f);
//     fflush(&f);
    return f;
}
inline FILE& operator<<(FILE& f, const __typeof__(clearbuff)&)
{
    fflush(&f);
    return f;
}

FILE& lin(*stdin);  // low-level-in
FILE& lout(*stdout);    // low-level-out
FILE& lerr(*stderr);

typedef pair<int,int> PII;

int main()
{
    int t;
    lin >> t;
    vector< int > r(150001, 0);
    for(int _i = 0; _i != t; ++_i)
    {
        int n;
        lin >> n;
        vector<string> w(n);
        for(int i = 0; i != n; ++i)
            lin >> w[i];
        vector<int> indegree(26), outdegree(26);
        vector<vector<bool> > adj(26, vector<bool>(26, false));
        vector<bool> has(26);
        int cntchars = 0;
        for(int i = 0; i != n; ++i)
        {
            if(!has[w[i][0] - 'a'])
            {
                ++cntchars;
                has[w[i][0] - 'a'] = true;
            }
            if(!has[w[i][w[i].size()-1] - 'a'])
            {
                ++cntchars;
                has[w[i][w[i].size()-1] - 'a'] = true;
            }
            ++outdegree[w[i][0] - 'a'];
            ++indegree[w[i][w[i].size()-1] - 'a'];
            adj[w[i][0]-'a'][w[i][w[i].size()-1]-'a'] = true;
            adj[w[i][w[i].size()-1]-'a'][w[i][0]-'a'] = true;
        }
        bool foundBeg = false, foundEnd = false;
        for(int i = 0; i != 26; ++i)
        {
            if(outdegree[i] == indegree[i])
                ;
            else if(outdegree[i] == indegree[i] + 1)
            {
                if(foundBeg)
                    goto fail;
                else
                    foundBeg = true;
            }
            else if(outdegree[i] + 1 == indegree[i])
            {
                if(foundEnd)
                    goto fail;
                else
                    foundEnd = true;
            }
            else
                goto fail;
        }
        if(foundBeg == foundEnd)
        {
            if(cntchars != 0)
            {
                vector<bool> visited(26);
                int s = 0;
                while(!has[s])
                    ++s;
                stack<int> tv; // to visit
                tv.push(s);
                while(!tv.empty())
                {
                    int x = tv.top();
                    tv.pop();
                    if(!visited[x])
                    {
                        visited[x] = true;
                        --cntchars;
                        for(int i = 0; i != 26; ++i)
                            if(!visited[i] && adj[x][i])
                                tv.push(i);
                    }
                }
                if(cntchars != 0)
                    goto fail;
            }
            lout << "Ordering is possible." << eol;
        }
        else
        {
            fail:
            lout << "The door cannot be opened." << eol;
        }
    }
}

