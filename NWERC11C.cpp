#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

class CountingTree
{
    struct treenode
    {
        int key;
        int priority;
        int cnt;
        treenode *left, *right, *parent;
        treenode(int key) : key(key), priority(rand()), cnt(1), left(0), right(0), parent(0)
        {
            priority /= 100001;
        }
        ~treenode()
        {
            if(left)
                delete left;
            if(right)
                delete right;
        }
    };
    treenode *root;
    void balance(treenode *n)
    {
        if(n->parent == 0)
            return;
        if(n->priority < n->parent->priority)
            return;
        treenode *parpar = n->parent->parent;
        n->parent->cnt -= n->cnt;
        if(n->parent->left == n)
        {
            n->parent->left = 0;
            n->right = merge(n->right, n->parent, n);
        }
        else
        {
            n->parent->right = 0;
            n->left = merge(n->parent, n->left, n);
        }
        if(parpar == 0)
            root = n;
        else
        {
            if(parpar->left == n->parent)
                parpar->left = n;
            else
                parpar->right = n;
        }
        n->cnt = (n->left == 0 ? 0 : n->left->cnt) + (n->right == 0 ? 0 : n->right->cnt) + 1;
        n->parent = parpar;
        balance(n);
    }
    void insertImpl(treenode *parent, treenode *&n, int key)
    {
        if(n == 0)
        {
            n = new treenode(key);
            n->parent = parent;
            balance(n);
        }
        else if(n->key == key) // nothing to do
            return;
        else
        {
            ++n->cnt;
            if(key < n->key)
                insertImpl(n, n->left, key);
            else
                insertImpl(n, n->right, key);
        }
    }
    treenode *merge(treenode *left, treenode *right, treenode *parent)
    {
        if(left == 0)
        {
            if(right != 0)
                right->parent = parent;
            return right;
        }
        if(right == 0)
        {
            if(left != 0)
                left->parent = parent;
            return left;
        }
        if(left->priority > right->priority)
        {
            left->parent = parent;
            left->right = merge(left->right, right, left);
            left->cnt = left->right->cnt + (left->left == 0 ? 0 : left->left->cnt) + 1;
            return left;
        }
        else
        {
            right->parent = parent;
            right->left = merge(left, right->left, right);
            right->cnt = right->left->cnt + (right->right == 0 ? 0 : right->right->cnt) + 1;
            return right;
        }
    }
    treenode *eraseImpl(treenode *n, int key)
    {
        if(n == 0)
            return 0;
        if(n->key == key)
        {
            treenode *left = n->left, *right = n->right, *parent = n->parent;
            n->parent = n->left = n->right = 0;
            delete n;
            return merge(left, right, parent);
        }
        else
        {
            --n->cnt;
            if(key < n->key)
                n->left = eraseImpl(n->left, key);
            else
                n->right = eraseImpl(n->right, key);
            return n;
        }
    }
    void printImpl(treenode *n)
    {
        if(n == 0)
            cerr << "()";
        else
        {
            cerr << n->key << "$" << n->priority << "#" << n->cnt << "[";
            printImpl(n->left);
            cerr << ",";
            printImpl(n->right);
            cerr << "]";
        }
    }
    void sortedImpl(treenode *n)
    {
        if(n == 0)
            return;
        cerr << "(";
        sortedImpl(n->left);
        cerr << "[" << n->key << "$" << n->priority << "#" << n->cnt << "]";
        sortedImpl(n->right);
        cerr << ")";
    }
    int countGreaterThanImpl(treenode *n, int key)
    {
        if(n == 0)
            return 0;
        else if(key >= n->key)
            return countGreaterThanImpl(n->right, key);
        else
            return (n->right == 0 ? 0 : n->right->cnt) + 1 + countGreaterThanImpl(n->left, key);
    }
    void checkConsistencyImpl(treenode *n, treenode *parent)
    {
        if(n == 0)
            return;
        if(n->parent != parent)
        {
            cerr << "Wrong parent in ";
            sortedImpl(n);
            cerr << "(" << n->parent << ":";
            sortedImpl(n->parent);
            cerr << ") (";
            sortedImpl(root);
            cerr << ")" << endl;
            return;
        }
        if(n->parent != 0 && n->priority >= n->parent->priority)
        {
            cerr << "Wrong priority in ";
            sortedImpl(n);
            cerr << endl;
        }
        checkConsistencyImpl(n->left, n);
        checkConsistencyImpl(n->right, n);
    }
public:
    CountingTree() : root(0)
    {}
    ~CountingTree()
    {
        if(root)
            delete root;
    }
    void insert(int key)
    {
        insertImpl(0, root, key);
    }
    void erase(int key)
    {
        root = eraseImpl(root, key);
    }
    void print()
    {
//         printImpl(root);
        sortedImpl(root);
        cerr << endl;
    }
    int countGreaterThan(int key)
    {
        return countGreaterThanImpl(root, key);
    }
    void checkConsistency()
    {
        checkConsistencyImpl(root, 0);
    }
};

int main()
{
//     CountingTree tr;
//     tr.insert(5);
//     tr.print();
//     tr.checkConsistency();
//     tr.insert(1);
//     tr.print();
//     tr.checkConsistency();
//     tr.insert(17);
//     tr.print();
//     tr.checkConsistency();
//     tr.insert(3);
//     tr.print();
//     tr.checkConsistency();
//     tr.insert(9);
//     tr.print();
//     tr.checkConsistency();
//     tr.insert(11);
//     tr.print();
//     tr.insert(7);
//     tr.print();
//     tr.insert(6);
//     tr.print();
//     tr.insert(4);
//     tr.print();
//     tr.erase(7);
//     tr.print();
//     tr.erase(5);
//     tr.print();
//     tr.checkConsistency();
    int _t;
    cin >> _t;
    for(int _i = 0; _i != _t; ++_i)
    {
        CountingTree tr;
        int m, r;
        cin >> m >> r;
        vector<int> lasttime(m);
        for(int i = 0; i != m; ++i)
        {
            tr.insert(m-i-1);
            lasttime[i] = m-i-1;
        }
        int t = m;
        for(int i = 0; i != r; ++i)
        {
            int a;
            cin >> a;
            --a;
            cout << (i==0?"":" ") << tr.countGreaterThan(lasttime[a]);
//             tr.checkConsistency();
//             tr.print();
            tr.erase(lasttime[a]);
//             tr.checkConsistency();
//             tr.print();
            tr.insert(lasttime[a] = t);
            ++t;
        }
//         tr.checkConsistency();
//         tr.print();
        cout << endl;
    }
}

