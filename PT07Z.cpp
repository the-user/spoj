#include <iostream>
#include <queue>
#include <vector>
using namespace std;

vector<vector<int> > adj;
vector<bool> visited;

pair<int, int> md(int x)
{
	if(visited[x])
		return make_pair(0, 0);
	visited[x] = true;
	pair<int,int> ret;
	ret.first = 0;
	ret.second = 0;
	int d1 = 0, d2 = 0;
	for(int i = 0; i != adj[x].size(); ++i)
	{
		if(!visited[adj[x][i]])
		{
		pair<int,int> t = md(adj[x][i]);
		if(t.first+1 > d1)
		{
			d2 = d1;
			d1 = t.first+1;
		}
		else if(t.first+1 > d2)
			d2 = t.first+1;
		if(t.second > ret.second)	
			ret.second = t.second;
		}
	}
	if(d2 + d1 > ret.second)
		ret.second = d2 + d1;
	ret.first = d1;
	return ret;
}



int main()
{
	int n;
	cin >> n;
	adj.resize(n);
	visited.resize(n, false);
	for(int i = 1; i != n; ++i)
	{
		int x, y;
		cin >> x >> y;
		--x;
		--y;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	cout << md(0).second << endl;
}


