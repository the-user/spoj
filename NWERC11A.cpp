#include <cstdio>
#include <vector>
#include <cmath>
#include <algorithm>
using namespace std;

typedef unsigned long long ull;

int main()
{
    int _t;
    scanf("%d", &_t);
    vector<ull> input(_t);
    for(int _i = 0; _i != _t; ++_i)
        scanf("%llu", &input[_i]);
    vector<vector<pair<ull, ull> > > solutions(_t);
    for(int _i = 0; _i != _t; ++_i)
    {
        ull x = input[_i];
        solutions[_i].push_back(make_pair(x, 1));
        solutions[_i].push_back(make_pair(x, x-1));
        if(ull(ceil(sqrt(double(2*x))))*(ull(ceil(sqrt(double(2*x))))-1) == 2*x)
        {
            solutions[_i].push_back(make_pair(ull(ceil(sqrt(double(2*x)))), 2));
            solutions[_i].push_back(make_pair(ull(ceil(sqrt(double(2*x)))), ull(ceil(sqrt(double(2*x))))-2));
        }
        ull approot3 = ull(ceil(pow(double(6*x), 1.0/3.0)));
        for(ull root3 = max(ull(3), approot3 - 1); root3 != approot3 + 2; ++root3)
        {
            if(root3*(root3-1)*(root3-2) == 6*x)
            {
                solutions[_i].push_back(make_pair(root3, 3));
                solutions[_i].push_back(make_pair(root3, root3-3));
            }
        }
        ull approot4 = ull(ceil(pow(double(24*x), 1.0/4.0)));
        for(ull root4 = max(ull(3), approot4 - 1); root4 != approot4 + 2; ++root4)
        {
            if(root4*(root4-1)*(root4-2)*(root4-3) == 24*x)
            {
                solutions[_i].push_back(make_pair(root4, 4));
                solutions[_i].push_back(make_pair(root4, root4-4));
            }
        }
        ull approot5 = ull(ceil(pow(double(120*x), 1.0/5.0)));
        for(ull root5 = max(ull(3), approot5 - 1); root5 != approot5 + 2; ++root5)
        {
            if(root5*(root5-1)*(root5-2)*(root5-3)*(root5-4) == 120*x)
            {
                solutions[_i].push_back(make_pair(root5, 5));
                solutions[_i].push_back(make_pair(root5, root5-5));
            }
        }
        ull approot6 = ull(ceil(pow(double(720*x), 1.0/6.0)));
        for(ull root6 = max(ull(3), approot6 - 1); root6 != approot6 + 2; ++root6)
        {
            if(root6*(root6-1)*(root6-2)*(root6-3)*(root6-4)*(root6-5) == 720*x)
            {
                solutions[_i].push_back(make_pair(root6, 6));
                solutions[_i].push_back(make_pair(root6, root6-6));
            }
        }
    }
    ull mx = *max_element(input.begin(), input.end());
    vector<ull> pascal;
    pascal.push_back(1);
    ull n = 0;
    if(mx > 6)
    {
        bool truncated = false;
        while(true)
        {
            ++n;
            vector<ull> old = pascal;
            for(ull i = 0; i != pascal.size(); ++i)
            {
                pascal[i] = (i == 0 ? 0 : old[i-1]) + old[i];
                for(int j = 0; j != _t; ++j)
                {
                    if(pascal[i] == input[j])
                    {
                        solutions[j].push_back(make_pair(n, i));
                        solutions[j].push_back(make_pair(n, n-i));
                    }
                }
            }
            while(pascal.back() >= mx)
            {
                truncated = true;
                pascal.pop_back();
            }
            if(!truncated && pascal.size() <= n/2 && pascal.back() < mx && n%2 == 0)
            {
                pascal.push_back(2*old.back());
                for(int j = 0; j != _t; ++j)
                {
                    if(pascal.back() == input[j])
                    {
                        solutions[j].push_back(make_pair(n, n/2));
                    }
                }
            }
            if(pascal.size() >= 8 && pascal[7] > mx || pascal.size() < 8 && n > 16)
                break;
        }
    }
    for(int i = 0; i != _t; ++i)
    {
        sort(solutions[i].begin(), solutions[i].end());
        solutions[i].resize(unique(solutions[i].begin(), solutions[i].end()) - solutions[i].begin());
        printf("%d\n", (int)solutions[i].size());
        for(int j = 0; j != solutions[i].size(); ++j)
        {
            if(j != 0)
                printf(" ");
            printf("(%llu,%llu)", solutions[i][j].first, solutions[i][j].second);
        }
        printf("\n");
    }
}
