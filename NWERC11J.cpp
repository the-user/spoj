#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <iomanip>
using namespace std;

struct connection
{
    int from, to, minute, d, t;
    double p;
};

const double eps = 0.000001;

int main()
{
    int _t;
    cin >> _t;
    for(int _i = 0; _i != _t; ++_i)
    {
        string origin, destination;
        cin >> origin >> destination;
        int n;
        cin >> n;
        map<string, int> nametonumber;
        vector<connection> connections;
        for(int i = 0; i != n; ++i)
        {
            string fromstr, tostr;
            cin >> fromstr >> tostr;
            if(!nametonumber.count(fromstr))
                nametonumber.insert(make_pair(fromstr, nametonumber.size()));
            if(!nametonumber.count(tostr))
                nametonumber.insert(make_pair(tostr, nametonumber.size()));
            int from = nametonumber[fromstr], to = nametonumber[tostr];
            connections.push_back(connection());
            connections.back().from = from;
            connections.back().to = to;
            cin >> connections.back().minute >> connections.back().t >> connections.back().p >> connections.back().d;
            connections.back().p /= 100; // percentage
        }
        if(!nametonumber.count(origin) || !nametonumber.count(destination))
        {
            cout << "IMPOSSIBLE" << endl;
            continue;
        }
        int orig = nametonumber[origin], dest = nametonumber[destination];
        vector<vector<double> > bestexpectedtime(nametonumber.size(), vector<double>(60, 1000000000.0));
        bestexpectedtime[dest] = vector<double>(60, 0.0);
        bool changed = true;
        vector<bool> updated(nametonumber.size(), false);
        updated[dest] = true;
        while(changed)
        {
            changed = false;
            vector<bool> newupdated(nametonumber.size(), false);
            for(int i = 0;  i != connections.size(); ++i)
            {
                connection c = connections[i];
                if(updated[c.to])
                {
                    double et = c.t + bestexpectedtime[c.to][(c.minute+c.t)%60]; // expected time
                    double extrat = 0.0;
                    for(int j = 1; j <= c.d; ++j)
                        extrat += c.t + j + bestexpectedtime[c.to][(c.minute+c.t+j)%60];
                    extrat /= c.d;
                    double newval = extrat * c.p + et * (1-c.p);
                    for(int j = c.minute; ; j == 0 ? j = 59 : --j)
                    {
                        if(newval < bestexpectedtime[c.from][j] - eps)
                        {
                            newupdated[c.from] = true;
                            changed = true;
                            bestexpectedtime[c.from][j] = newval;
                        }
                        else
                            break;
                        ++newval;
                    }
                }
            }
            updated = newupdated;
        }
        if(bestexpectedtime[orig][0] >= 1000000000 - eps)
            cout << "IMPOSSIBLE" << endl;
        else
            cout << setprecision(12) << *min_element(bestexpectedtime[orig].begin(), bestexpectedtime[orig].end()) << endl;
    }
}