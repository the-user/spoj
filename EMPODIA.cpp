#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>
#include <stack>
#include <vector>
#include <deque>
#include <cmath>
#include <algorithm>
using namespace std;

typedef unsigned long long ull;

inline FILE& operator>>(FILE& f, char*& d)
{
    int s = 20;
    d = (char*)malloc(s);
    int chr;
    int i = 0;
    do
    {
        chr = fgetc(&f);
        if(chr == EOF)
            goto OPERATOR_RSHIFT_FILE_CHAR_PTR_end;
    }
    while(chr == '\n' || chr == '\r' || chr == '\t' || chr == ' ');
    do
    {
        if(i == s)
        {
            s *= 2;
            d = (char*)realloc(d, s);
        }
        d[i] = chr;
        chr = fgetc(&f);
        ++i;
    }
    while(chr != EOF && chr != '\n' && chr != '\r' && chr != '\t' && chr != ' ');
    OPERATOR_RSHIFT_FILE_CHAR_PTR_end:;
    d = (char*)realloc(d, i+1);
    d[i] = '\0';
    return f;
}
inline FILE& operator>>(FILE& f, char& chr)
{
    int x;
    do
    {
        x = fgetc(&f);
        if(x == EOF)
        {
            chr = '\0';
            return f;
        }
    }
    while(x == '\n' || x == '\r' || x == '\t' || x == ' ');
    chr = x;
    return f;
}
inline FILE& operator>>(FILE& f, int& x)
{
    char *d;
    f >> d;
    x = atoi(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, ull& x)
{
    fscanf(&f, "%llu", &x);
    return f;
}
inline FILE& operator>>(FILE& f, float& x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, double& x)
{
    char *d;
    f >> d;
    x = atof(d);
    free(d);
    return f;
}
inline FILE& operator>>(FILE& f, long double& x)
{
    fscanf(&f, "%LE", &x);
    return f;
}
inline FILE& operator>>(FILE& f, string& str)
{
    char *d;
    f >> d;
    str.~string();
    new (&str) string(d);
    free(d);
    return f;
}
inline FILE& operator<<(FILE& f, const char *str)
{
    fputs(str, &f);
    return f;
}
inline FILE& operator<<(FILE& f, int x)
{
    fprintf(&f, "%d", x);
    return f;
}
inline FILE& operator<<(FILE& f, size_t x)
{
    fprintf(&f, "%u", x);
    return f;
}
inline FILE& operator<<(FILE& f, ull x)
{
    fprintf(&f, "%llu", x);
    return f;
}
inline FILE& operator<<(FILE& f, double x)
{
    fprintf(&f, "%.12f", x);
    return f;
}
inline FILE& operator<<(FILE& f, long double x)
{
    fprintf(&f, "%.12Lf", x);
    return f;
}
inline FILE& operator<<(FILE& f, const string& str)
{
    f << str.c_str();
    return f;
}
inline FILE& operator<<(FILE& f, char c)
{
    fputc(c, &f);
    return f;
}
struct _endofline
{
} eol;
struct _flush
{
} clearbuff;
inline FILE& operator<<(FILE& f, const __typeof__(eol)&)
{
    fputc('\n', &f);
//     fflush(&f);
    return f;
}
inline FILE& operator<<(FILE& f, const __typeof__(clearbuff)&)
{
    fflush(&f);
    return f;
}

FILE& lin(*stdin);  // low-level-in
FILE& lout(*stdout);    // low-level-out
FILE& lerr(*stderr);

typedef pair<int,int> PII;

int main()
{
    int t;
    lin >> t;
    vector<int> a; // Array
    deque<PII> b; // Buffer
    deque<PII> bx; // Buffer max
    vector<PII> r; // Result
    for(int _i = 0; _i != t; ++_i)
    {
        a.clear();
        b.clear();
        bx.clear();
        r.clear();
        int n;
        lin >> n;
        a.resize(n);
        for(int i = 0; i != n; ++i)
            lin >> a[i];
        for(int i = 0; i != n; ++i)
        {
            int x = a[i];
            // Gesucht: (x - p, i - p)
            int j1 = b.size() - 1, j2 = bx.size() - 1;
            while(j1 >= 0)
            {
                if(j2 != -1 && bx[j2].second > b[j1].second)
                {
                    if(bx[j2].first > x)
                        break;
                    --j2;
                }
                else
                {
                    if((x - b[j1].first) == (i - b[j1].second))
                    {
                        PII d = b[j1];
                        b.erase(b.begin(), b.begin() + j1 + 1);
                        r.push_back(make_pair(d.second, i));
                        b.push_back(make_pair(a[i], i));
                        break;
                    }
                    --j1;
                }
            }
            b.erase(upper_bound(b.begin(), b.end(), make_pair(a[i], i)), b.end());
            bx.erase(upper_bound(bx.begin(), bx.end(), make_pair(a[i], i), greater<PII>()), bx.end());
            b.push_back(make_pair(a[i], i));
            bx.push_back(make_pair(a[i], i));
            done:;
        }
        lout << r.size() << '\n';
        for(int i = 0; i != r.size(); ++i)
            lout << r[i].first+1 << ' ' << r[i].second + 1 << '\n';
    }
}

